#!/usr/bin/env python3
# coding: utf-8

import numpy as np
import matplotlib.pyplot as plt
from matplotlib.colors import LinearSegmentedColormap
from PIL import Image
import imageio

def create_mapping(cmap_name, blue=False):
    cmap = plt.cm.get_cmap(cmap_name)
    mapping = dict()
    resolution = 8
    CUT = 0.1
    RMIN = CUT*cmap.N
    RMAX = cmap.N
    VMAX = 256*resolution
    for value in range(0, VMAX+1):
        t = 2 * value / VMAX - 1
        r = 255 * np.clip(1.5 - np.abs(2 * t - 1), 0, 1)
        g = 255 * np.clip(1.5 - np.abs(2 * t), 0, 1)
        b = 255 * np.clip(1.5 - np.abs(2 * t + 1), 0, 1)
        a = 255
        rgba = int(r) + 256*int(g) + 256*256*int(b) + 256*256*256*int(a)
        v01 = value/VMAX
        index = RMIN + (RMAX-RMIN)*v01
        remap = cmap(int(index))
        mapping[rgba] = [int(255*remap[0]), int(255*remap[1]), int(255*remap[2]), 255]
    mapping[255 + 256*255 + 256*256*255 + 0] = [0, 0, 0, 255]
    if blue:
        mapping[0 + 256*0 + 256*256*127 + 0] = [0, 0, 0, 255]
    return mapping

def remap(image, mapping):
    for i in range(0, image.shape[0]):
        for j in range(0, image.shape[1]):
            val = image[i,j]
            image[i,j] = mapping[val[0] + 256*val[1] + 256*256*val[2] + 256*256*256*val[3]]

    return Image.fromarray(image, 'RGBA')

def convert(filename, mapping):
    image = remap(imageio.imread(filename), mapping)
    image.save(filename[:-4] + '_remapped.png')    



mapping = create_mapping('cubehelix')
filenames = ['CB_2020.tif_max_128_local_b.png', 'Havirov_2020.tif_max_128_local_b.png']

for f in filenames:
    print(f)
    convert(f, mapping)
    print('done')

mapping = create_mapping('cubehelix', blue=True)
filenames = ['CB_2020.tif_max_128_local_space.png', 'Havirov_2020.tif_max_128_local_space.png']

for f in filenames:
    print(f)
    convert(f, mapping)
    print('done')
    



