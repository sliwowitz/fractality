# fractality

Python script for calculating the box counting (BC) dimension of grayscale images.

Suitable for batch processing of many images. 

Main modes of operation
 
 * global: outputs a single number representing the BC dimension
 * local: outputs an image where each pixel is colored according
   to the BC dimension of a box of a specific size centered at the pixel
 * partial: outputs an image divided to subgrid of specified size colored
   by the BC dimension of each grid cell
 * difference: outptus an image colored according to a difference between
   partial dimansions of two input images

The input images can be cropped to their bounding boxes (default) or processed
including possible empty border areas. Window size can be selected for 
the local/partial mode. 100% full boxes can be optionally counted as a positive
hit. Individual log counts and log bix sizes can be outptu as raw data 
before fitting.

