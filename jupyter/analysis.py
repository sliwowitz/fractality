import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import glob, os
from pathlib import Path
import matplotlib as mpl

FIT_TO_MAXVAL = True

def power_function(x, a, b):
    return np.exp(a) * np.power(x, b)

class CityDistribution:
    name = None
    bins = None
    freq = None
    
    idx1 = None
    idx2 = None
    idx3 = None
    
    fit_a1 = None
    fit_b1 = None
    fit_a2 = None
    fit_b2 = None
    fit_a3 = None
    fit_b3 = None
    
    freq_fit1 = None
    freq_fit2 = None
    freq_fit3 = None

    def __init__(self, histogram, settings):
        self.name = histogram.name
        self.bins = histogram.index.to_numpy()
        self.freq = histogram.to_numpy(dtype='float64')
        self.settings = settings
        self.process()
        
    def process(self):
        idx = (self.freq > 0) & (self.freq >= self.settings["min_stat"])
        if FIT_TO_MAXVAL:
            idx = (idx) & (self.bins < self.settings["max_size"])
        self.idx1 = (idx) & (self.bins < self.settings["mid_separation"])
        self.idx2 = (idx) & (self.bins >= self.settings["mid_separation"]) & (self.bins < self.settings["large_separation"])
        self.idx3 = (idx) & (self.bins >= self.settings["large_separation"])
        
        if np.any(self.idx1):
            [self.fit_b1, self.fit_a1] = np.polyfit(np.log(self.bins[self.idx1]), np.log(self.freq[self.idx1]), 1)
            self.freq_fit1 = power_function(self.bins, self.fit_a1, self.fit_b1)
        if np.any(self.idx2):
            [self.fit_b2, self.fit_a2] = np.polyfit(np.log(self.bins[self.idx2]), np.log(self.freq[self.idx2]), 1)
            self.freq_fit2 = power_function(self.bins, self.fit_a2, self.fit_b2)
        if np.any(self.idx3):
            [self.fit_b3, self.fit_a3] = np.polyfit(np.log(self.bins[self.idx3]), np.log(self.freq[self.idx3]), 1)
            self.freq_fit3 = power_function(self.bins, self.fit_a3, self.fit_b3)

        
    def diff(self, cutoff=0, maxval=np.inf):
        diff = np.full_like(self.freq, np.nan)
        if np.any(self.idx1):
            diff[self.idx1] = self.freq[self.idx1] - self.freq_fit1[self.idx1]
        if np.any(self.idx2):
            diff[self.idx2] = self.freq[self.idx2] - self.freq_fit2[self.idx2]
        if np.any(self.idx3):
            diff[self.idx3] = self.freq[self.idx3] - self.freq_fit3[self.idx3]
        return diff
                
    def logdev(self, cutoff=0, maxval=np.inf):
        diff = self.diff(cutoff, maxval)
        diff = diff[~np.isnan(diff)]
        logdiff = np.sign(diff) * np.log(np.abs(diff))
        return np.std(logdiff)
        

def get_histograms(data, min_size, bin_size):
    largest_object = int(np.max(data.max()))
    bins = range(0, largest_object+bin_size, bin_size) # Define bins to span up to the largest object in the whole data set
    histograms = pd.DataFrame(index=bins[1:])         # Bins edges are inclusive, but the histogram data is not
    for col in data:
        col_data = data[col].to_numpy().transpose()
        col_data = col_data[~np.isnan(col_data)]
        hist = np.histogram(col_data, bins=bins)
        histograms[col] = hist[0]
    return histograms
    
def process_data(data, settings):
    histograms = get_histograms(data, settings["min_size"], settings["bin_size"])
    cities = dict()
    for h in histograms:
        cities[h] = CityDistribution(histograms[h], settings)
    settings["MAX_Y"] = int(np.max(histograms.max()))
    return cities
      
def plot_linear(city, settings):
    linear = plt.figure()
    ax_linear = linear.add_axes([0, 0, 1, 1])
    if np.any(city.idx1):
        ax_linear.plot(city.bins[city.idx1], city.freq[city.idx1], 'o')
    if np.any(city.idx2):
        ax_linear.plot(city.bins[city.idx2], city.freq[city.idx2], 'o')
    if np.any(city.idx3):
        ax_linear.plot(city.bins[city.idx3], city.freq[city.idx3], 'o')
        
    if np.any(city.idx1):
        ax_linear.plot(city.bins, city.freq_fit1, '-')
    if np.any(city.idx2):
        ax_linear.plot(city.bins, city.freq_fit2, '-')
    if np.any(city.idx3):
        ax_linear.plot(city.bins, city.freq_fit3, '-')
        
        
    ax_linear.set_xlabel("volume [$\mathrm{m}^3$]")
    ax_linear.set_ylabel("frequency [arb. u.]")
    
    ax_linear.set_xticks(city.bins)
    ax_linear.tick_params(axis='x', labelrotation=90)
    
    ax_linear.set_xlim(0, settings["max_size"])
    ax_linear.set_ylim(0, settings["MAX_Y"])
    
    linear.title = city.name
    return linear

def plot_two_linear(city1, city2, settings):
    linear = plt.figure()
    ax_linear = linear.add_axes([0, 0, 1, 1])
    if np.any(city1.idx1):
        ax_linear.plot(city1.bins[city1.idx1], city1.freq[city1.idx1], 'o')
    if np.any(city1.idx2):
        ax_linear.plot(city1.bins[city1.idx2], city1.freq[city1.idx2], 'o')
    if np.any(city1.idx3):
        ax_linear.plot(city1.bins[city1.idx3], city1.freq[city1.idx3], 'o')
        
    if np.any(city1.idx1):
        ax_linear.plot(city1.bins, city1.freq_fit1, '-')
    if np.any(city1.idx2):
        ax_linear.plot(city1.bins, city1.freq_fit2, '-')
    if np.any(city1.idx3):
        ax_linear.plot(city1.bins, city1.freq_fit3, '-')
        
    if np.any(city2.idx1):
        ax_linear.plot(city2.bins[city2.idx1], city2.freq[city2.idx1], 'x')
    if np.any(city2.idx2):
        ax_linear.plot(city2.bins[city2.idx2], city2.freq[city2.idx2], 'x')
    if np.any(city2.idx3):
        ax_linear.plot(city2.bins[city2.idx3], city2.freq[city2.idx3], 'x')
        
    if np.any(city2.idx1):
        ax_linear.plot(city2.bins, city2.freq_fit1, '-')
    if np.any(city2.idx2):
        ax_linear.plot(city2.bins, city2.freq_fit2, '-')
    if np.any(city2.idx3):
        ax_linear.plot(city2.bins, city2.freq_fit3, '-')
        
        
    ax_linear.set_xlabel("volume [$\mathrm{m}^3$]")
    ax_linear.set_ylabel("frequency [arb. u.]")
    
    ax_linear.set_xticks(city1.bins)
    ax_linear.tick_params(axis='x', labelrotation=90)
    
    ax_linear.set_xlim(0, settings["max_size"])
    ax_linear.set_ylim(0, 10000)
    
    linear.title = [city1.name + ', ' + city2.name]
    return linear

def plot_loglog(city, settings):
    loglog = plt.figure()
    ax_loglog = loglog.add_axes([0, 0, 1, 1])
    
    if np.any(city.idx1):
        ax_loglog.loglog(city.bins[city.idx1], city.freq[city.idx1], 'o')
    if np.any(city.idx2):
        ax_loglog.loglog(city.bins[city.idx2], city.freq[city.idx2], 'o')
    if np.any(city.idx3):
        ax_loglog.loglog(city.bins[city.idx3], city.freq[city.idx3], 'o')
        
    if np.any(city.idx1):
        ax_loglog.loglog(city.bins[city.idx1], city.freq_fit1[city.idx1], '-')
    if np.any(city.idx2):
        ax_loglog.loglog(city.bins[city.idx2], city.freq_fit2[city.idx2], '-')
    if np.any(city.idx3):
        ax_loglog.loglog(city.bins[city.idx3], city.freq_fit3[city.idx3], '-')
        
    loglog.title = city.name
    
    ax_loglog.set_xlabel("volume [$\mathrm{m}^3$]")
    ax_loglog.set_ylabel("frequency [arb. u.]")
    
    ax_loglog.set_xlim(0.8*settings["min_size"], settings["max_size"])
    ax_loglog.set_ylim(1, 1.2*settings["MAX_Y"])
    return loglog

def plot_stddev(city, settings):
    stddev = plt.figure()
    ax_stddev = stddev.add_axes([0, 0, 1, 1])
    ax_stddev.semilogx(city.bins, np.log(np.abs(city.diff(settings["min_stat"], settings["max_size"]))), 'x')
    stddev.title = city.name + " stddev = " + str(city.logdev(settings["min_stat"], settings["max_size"]))
    
    ax_stddev.set_xlabel("volume [$\mathrm{m}^3$]")
    ax_stddev.set_ylabel("$\sigma_{\log(f)}^2$ [arb. u.]")
    
    ax_stddev.set_xlim(0.8*settings["min_size"], settings["max_size"])
    return stddev

def plot_city(city, settings):
    plot_linear(city, settings)
    plot_loglog(city, settings)
    plot_stddev(city, settings)
        
def export_cities(cities, settings, export_type, prefix="export/"):
    for c in cities:
        city = cities[c]
        linear = plot_linear(city, settings)
        linear.savefig(prefix + city.name + " - linear." + export_type, bbox_inches='tight', pad_inches=0, dpi=600)
        plt.close(linear)
        loglog = plot_loglog(city, settings)
        loglog.savefig(prefix + city.name + " - loglog." + export_type, bbox_inches='tight', pad_inches=0, dpi=600)
        plt.close(loglog)
        stddev = plot_stddev(city, settings)
        stddev.savefig(prefix + city.name + " - stddev." + export_type, bbox_inches='tight', pad_inches=0, dpi=600)
        plt.close(stddev)
        
def gstats_cities(cities, settings):
    gstats = dict()
    for city_name in cities:
#        highlow = cities[city_name].fit_b1 - cities[city_name].fit_b2
        logdev = cities[city_name].logdev(settings["min_stat"], settings["max_size"])
        low = -cities[city_name].fit_b1 if cities[city_name].fit_b1 else None
        mid = -cities[city_name].fit_b2 if cities[city_name].fit_b2 else None
        high = -cities[city_name].fit_b3 if cities[city_name].fit_b3 else None
        gstats[city_name] = np.array([logdev, low, mid, high])
    odchylka = pd.DataFrame.from_dict(gstats, orient='index', columns=["Log. std. dev.", "Low", "Mid", "High"])
    return odchylka

