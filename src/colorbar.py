#!/usr/bin/python3
import math

import matplotlib
matplotlib.use('Agg')
import scipy.misc
import scipy.ndimage
import numpy as np
import matplotlib.pyplot as plt
from PIL import Image

import os
import argparse
import progressbar
from fnmatch import fnmatch

MIN = 1.0
MAX = 1.8

mp = np.zeros((50,800,3), dtype=np.uint8)
for j in range(0,mp.shape[1]):
    t = 2*j/mp.shape[1]-1
    for i in range(0,mp.shape[0]):
        mp[i, j, 0] = 255 * np.clip(1.5 - np.abs(2 * t - 1), 0, 1)
        mp[i, j, 1] = 255 * np.clip(1.5 - np.abs(2 * t), 0, 1)
        mp[i, j, 2] = 255 * np.clip(1.5 - np.abs(2 * t + 1), 0, 1)

img = Image.fromarray(mp ,'RGB')
img.save("colorbar.png")

