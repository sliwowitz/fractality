#!/usr/bin/env python3

import scipy.misc
import scipy.ndimage
import numpy as np
from PIL import Image
import imageio

import os
import sys
import argparse
import progressbar
import math
from fnmatch import fnmatch
from matplotlib import cm
from scipy.stats import linregress

parser = argparse.ArgumentParser(description='Calculate Minkowski-Bouligand dimension for a series of images')
parser.add_argument('filename', help='File names to parse', nargs='*')
parser.add_argument('--local', dest='local', action='store_const',
                    const=True, default=False,
                    help='Count the local dimension (default: global)')
parser.add_argument('--partial', dest='partial', action='store_const',
                    const=True, default=False,
                    help='Count the partial grid dimension (overridden by local)')
parser.add_argument('--no-crop', dest='crop', action='store_const',
                    const=False, default=True,
                    help="Don't crop to city boundaries (default: crop)")
parser.add_argument('--debug', dest='debug', action='store_const',
                    const=False, default=False,
                    help="Print debugging information (default: no)")
parser.add_argument('--window', default=64, type=int,
                    help='Maximum local window size (default: 64)', nargs='?')
parser.add_argument('--difference', dest='difference', action='store_const',
                    const=True, default=False,
                    help='Count dimension difference between two pictures. Valid for partial dimension only')
parser.add_argument('--count-full', dest='count_full', action='store_const',
                    const=True, default=False,
                    help="Count 100%% full boxes as a positive hit (default: no)")
parser.add_argument('--output-log-levels', dest='output_log_levels', action='store_const',
                    help="Show the actaul box counts and sizes",
                    const=True, default=False)
parser.add_argument('--output-stats-file', dest='stats_file', action='store_const',
                    help="Write results to a file complete with regression statistics",
                    const=True, default=False)
parser.add_argument('--append-full-path', dest='append_full_path', action='store_const',
                    help="Append full path to the output file name",
                    const=True, default=False)
args = parser.parse_args()


def debug(msg):
    if args.debug:
        print(msg)


def print_binary_image(name, img):
    scipy.misc.imsave(name + '.bmp', np.array(img, dtype=int))


# Adapted from https://github.com/rougier/numpy-100 (#87)
if args.count_full:
    def boxcount_section(binary_img, k, xmin, xmax, ymin, ymax):
        S = np.add.reduceat(np.add.reduceat(binary_img,
                                            np.arange(xmin, xmax, k),
                                            axis=0),
                            np.arange(ymin, ymax, k),
                            axis=1)

        # Count non-empty (0) boxes
        return len(np.where(S > 0)[0])
else:
    def boxcount_section(binary_img, k, xmin, xmax, ymin, ymax):
        S = np.add.reduceat(np.add.reduceat(binary_img,
                                            np.arange(xmin, xmax, k),
                                            axis=0),
                            np.arange(ymin, ymax, k),
                            axis=1)

        # Count non-empty (0) and non-full boxes (k*k)
        return len(np.where((S > 0) & (S < k * k))[0])


def boxcount(binary_img, k):
    return boxcount_section(binary_img, k, 0, binary_img.shape[0], 0, binary_img.shape[1])


def fractal_dimension(binary_image, box_sizes, stats=False):
    # Actual box counting with decreasing size
    counts = np.zeros(box_sizes.shape, dtype=int)
    it = np.nditer(counts, flags=['multi_index'], op_flags=['writeonly'])
    while not it.finished:
        it[0] = boxcount(binary_image, box_sizes[it.multi_index])
        it.iternext()

    # Fit the successive log(sizes) with log(counts)
    idx = (counts != 0)
    box_sizes = box_sizes[idx]
    counts = counts[idx]
    if len(counts) <= 1:  # Completely empty image or just one hit from the largest box
        return 0
    else:
        if args.output_log_levels:  # TODO: Global variable
            print("---\nbox_size, count")
            for i in range(0, len(box_sizes)):
                print(box_sizes[i], ", ", counts[i])  # TODO: Superfluous comma after first element
            print("---\n")
        if stats:
            slope, intercept, r_value, p_value, std_err = scipy.stats.linregress(np.log(box_sizes), np.log(counts))
            dof = len(box_sizes) - 2  # linear regression dof = n - 2
            return [-slope, r_value, p_value, std_err, dof]
        else:
            return -scipy.stats.linregress(np.log(box_sizes), np.log(counts))[0]


def build_box_sizes(max_box_size, min_box_base=2):
    # Greatest power of 2 less than or equal to maximum allowed box size
    n = 2 ** np.floor(np.log(max_box_size) / np.log(2))
    n = int(np.log(n) / np.log(2))  # Extract the exponent

    # Build successive box sizes (from 2**n down to 2**min_box_base inclusive)
    sizes = 2 ** np.arange(n, min_box_base - 1, -1)
    debug("Box sizes: " + str(sizes[0]) + " - " + str(sizes[-1]))
    return np.array(sizes)


def crop_image(img):
    nz = img.nonzero()  # Find the indices of non-zero elements
    img = img[min(nz[0]):max(nz[0]), min(nz[1]):max(nz[1])]  # Crop to non-zero part only
    return img


def normalize_image(img, padding=0, threshold=0.9, crop=True):
    assert len(img.shape) == 2, "Input must be a 2D grayscale image."

    binary_img = (img < threshold)  # Transform Z into a binary array
    if crop:
        binary_img = crop_image(binary_img)
    binary_img = np.pad(binary_img, pad_width=padding, mode='constant')  # Pad image with zeros

    return binary_img


def map_to_image(image_map, mask=None, MIN=1.0, MAX=1.8):
    
    image = np.zeros(image_map.shape + (4,), dtype=np.uint8)

    value = np.clip(image_map, MIN, MAX)
    t = (2 / (MAX - MIN)) * (value - MIN) - 1

    image[:, :, 0] = 255 * np.clip(1.5 - np.abs(2 * t - 1), 0, 1)
    image[:, :, 1] = 255 * np.clip(1.5 - np.abs(2 * t), 0, 1)
    image[:, :, 2] = 255 * np.clip(1.5 - np.abs(2 * t + 1), 0, 1)
    image[:, :, 3] = np.where(image_map > 0, 255, 0)

    return Image.fromarray(image, 'RGBA')


def difference_map(part0, part1, basename):
    out_map = np.array(part0 - part1)
    out_map[part0 == 1.0] = np.nan
    out_map[part1 == 1.0] = np.nan
    np.save(basename, out_map)
    return out_map


def difference_map_to_image(image_map, mask=None, MIN=-2.0, MAX=2.0):

    # image = np.zeros(image_map.shape + (4,), dtype=np.uint8)
    # # TODO: Easily vectorized
    # widgets = [progressbar.Percentage(),
    #            ' ',
    #            progressbar.Bar(marker=progressbar.RotatingMarker()),
    #            ' ',
    #            progressbar.ETA()]
    # pbar = progressbar.ProgressBar(widgets=widgets, maxval=image_map.shape[0] - 1).start()
    # for i in range(0, image_map.shape[0]):
    #     pbar.update(i)
    #     for j in range(0, image_map.shape[1]):
    #         value = np.clip(image_map[i, j], MIN, MAX)
    #         t = (value - MIN) * 2 / (MAX - MIN) - 1
    #         image[i, j, 0] = 255 * np.clip(1.5 - np.abs(2 * t - 1), 0, 1)
    #         image[i, j, 1] = 255 * np.clip(1.5 - np.abs(2 * t), 0, 1)
    #         image[i, j, 2] = 255 * np.clip(1.5 - np.abs(2 * t + 1), 0, 1)
    #         image[i, j, 3] = 255
    #
    # pbar.finish()

    clipped = np.clip(image_map, MIN, MAX)
    normalized = (clipped - MIN) / (MAX - MIN)
    image = np.uint8(255 * cm.seismic(normalized))
    image[mask == 0] = [0, 0, 0, 255]
    return Image.fromarray(image)


def standard_dimension(img, crop, stats=False):
    binary_image = normalize_image(img, crop=crop)
    box_sizes = build_box_sizes(min(img.shape))  # Maximum allowed box size is the lesser of image dimensions
    return fractal_dimension(binary_image, box_sizes, stats)


def local_dimension(img, max_size, crop):
    max_size = max_size if max_size % 2 == 0 else max_size + 1  # max_size must be even
    box_sizes = build_box_sizes(max_size)
    max_size = max(box_sizes)
    padding = int(max_size / 2)
    binary_image = normalize_image(img, padding=padding, crop=crop)
    values = np.ones(binary_image.shape, dtype=float)

    widgets = [progressbar.Percentage(),
               ' ',
               progressbar.Bar(marker=progressbar.RotatingMarker()),
               ' ',
               progressbar.ETA()]
    pbar = progressbar.ProgressBar(widgets=widgets, maxval=values.shape[0] - 2 * padding).start()

    # TODO: Better way of getting the indexes and iterating. If the sub_img index goes out of bounds, strange things happen.
    imin = padding
    imax = values.shape[0] - padding
    jmin = padding
    jmax = values.shape[1] - padding

    for i in range(imin, imax):
        pbar.update(i - padding)
        for j in range(jmin, jmax):
            sub_img = binary_image[i - padding:i + padding, j - padding:j + padding]
            values[i, j] = fractal_dimension(sub_img, box_sizes)

    pbar.finish()
    values = scipy.ndimage.uniform_filter(values, size=5, mode='constant')
    values[binary_image == 0] = 0  # TODO: more pythonic way?

    return values[imin:imax, jmin:jmax]


def partial_dimension(img, max_size, crop, drop_zeros=True):
    box_sizes = build_box_sizes(max_size)
    binary_image = normalize_image(img, crop=crop)

    xvalues = np.arange(0, binary_image.shape[0] + 1, max_size)
    yvalues = np.arange(0, binary_image.shape[1] + 1, max_size)

    result = np.zeros(binary_image.shape, dtype=float)

    widgets = [progressbar.Percentage(),
               ' ',
               progressbar.Bar(marker=progressbar.RotatingMarker()),
               ' ',
               progressbar.ETA()]
    pbar = progressbar.ProgressBar(widgets=widgets, maxval=len(xvalues) - 1).start()

    for i in range(0, len(xvalues) - 1):
        pbar.update(i)
        for j in range(0, len(yvalues) - 1):
            sub_img = binary_image[xvalues[i]:xvalues[i + 1], yvalues[j]:yvalues[j + 1]]
            result[xvalues[i]:xvalues[i + 1], yvalues[j]:yvalues[j + 1]] = fractal_dimension(sub_img, box_sizes)

    pbar.finish()
    if drop_zeros:
        result[binary_image == 0] = 0

    return result


def list_files():
    root = './plans'
    pattern = "*.tif"

    filenames = []

    for path, subdirs, files in os.walk(root):
        for name in files:
            if fnmatch(name, pattern):
                filenames.append(os.path.join(path, name))

    return filenames


###############################################################################

if len(args.filename) > 0:
    files = args.filename
else:
    files = list_files()

MAX_WINDOW_SIZE = args.window
if MAX_WINDOW_SIZE < 4:
    print("ERROR: Window size must be at least 4")
    sys.exit(11)
if not (math.log2(MAX_WINDOW_SIZE).is_integer()):
    print("Warning: Window size not a power of 2. Nearest lower power of two will be used instead")

crop = args.crop  # TODO: Move image manipulation to an object which would be set at instantiation
#                           so that we don't have to pass the crop argument around.

if args.difference:
    if not args.partial:
        print("Difference counting is implemented only for partial dimension")
        sys.exit(10)
    if args.crop:
        print("WARNING: Crop setting ignored for difference map.")
    if len(args.filename) != 2:
        print("You must provide exactly two files for difference counting")

    image0 = imageio.imread(files[0]) / 255.0
    part0 = partial_dimension(image0, MAX_WINDOW_SIZE, crop=False, drop_zeros=False)
    image1 = imageio.imread(files[1]) / 255.0
    part1 = partial_dimension(image1, MAX_WINDOW_SIZE, crop=False, drop_zeros=False)

    basename = files[0] + '_grid_' + str(MAX_WINDOW_SIZE) + '_difference'

    out_map = difference_map(part0, part1, basename)

    out_image = difference_map_to_image(out_map, mask=image0, MIN=-0.08, MAX=0.08)
    out_name = basename + '.png'
    out_image.save(out_name)

else:
    if args.local:
        debug("Local dimension calculation mode")
        for f in files:
            image = imageio.imread(f) / 255.0
            loc = local_dimension(image, MAX_WINDOW_SIZE, crop=crop)

            out_image = map_to_image(loc)

            fn = fname if args.append_full_path else os.path.basename(f)
            out_name = fn + '_max_' + str(MAX_WINDOW_SIZE) + '_local.png'
            out_image.save(out_name)

    elif args.partial:
        debug("Partial dimension calculation mode")
        for f in files:
            image = imageio.imread(f) / 255.0
            part = partial_dimension(image, MAX_WINDOW_SIZE, crop=crop)

            out_image = map_to_image(part)

            fn = fname if args.append_full_path else os.path.basename(f)
            out_name = fn + '_grid_' + str(MAX_WINDOW_SIZE) + '_partial.png'
            out_image.save(out_name)

    else:
        debug("Global dimension calculation mode")
        if args.stats_file:
            f = open("dimension.csv", "x")
            f.write("city, year, dimension, r^2, p, std_err, dof\n")
        for i in range(0, len(files)):
            print("Processing ", i, "/", len(files), ": ", files[i])
            image = imageio.imread(files[i]) / 255.0
            if args.stats_file:
                box_dimension, r_value, p_value, std_err, deg_of_freedom = standard_dimension(image, crop=crop, stats=True)

                debug("Minkowski-Bouligand dimension: " + str(box_dimension))

                # Write result to file
                fname = os.path.basename(files[i])
                fname = fname[0:len(fname) - 4]
                fsplit = fname.split('_')
                city = fsplit[0]
                year = fsplit[1]
                f.write(city + ", " + year
                        + ", " + str(box_dimension)
                        + ", " + str(r_value ** 2)
                        + ", " + str(p_value)
                        + ", " + str(std_err)
                        + ", " + str(deg_of_freedom)
                        + "\n")
                f.flush()
            else:
                box_dimension = standard_dimension(image, crop=crop, stats=False)
                print(os.path.basename(files[i]), ": ", box_dimension)

